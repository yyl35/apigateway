"""djangoProject1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from soc import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('index/', views.vendor_cards, name='vendor_cards'),
    path('create_service/', views.create_service, name='create_service'),
    path('createservice/', views.create_service_instance),
    path('fetch_data/', views.fetch_data, name='fetch_data'),
    path('display_data/', views.display_data, name='display_data'),
    path('update_data/', views.update_data, name='update_data'),
    path('page_3.html/', views.page_3_view, name='page_3'),
    path('login/', views.login_view, name='login'),
    path('admin/', admin.site.urls),
    path('', views.ecoss, name='ecoss'),
    path('create_instance/', views.create_instance2, name='create_instance'),

]