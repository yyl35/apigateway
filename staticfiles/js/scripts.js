function showOptions(vendorName) {
    const options = {
        'Fortinet': [
            { name: '阿里云', logo: '/static/images/aliyun-logo.png' },
            { name: '天翼云', logo: '/static/images/tianyiyun-logo.png' }
        ],
        '万博': [
            { name: '阿里云', logo: '/static/images/aliyun-logo.png' },
            { name: '天翼云', logo: '/static/images/tianyiyun-logo.png' }
        ]
    };

    const content = document.getElementById('cloud-options-content');
    content.innerHTML = '';

    options[vendorName].forEach(option => {
        content.innerHTML += `
            <div class="card">
                <img src="${option.logo}" alt="${option.name} logo">
            </div>
        `;
    });

    document.getElementById('cloud-options').style.display = 'block';
}

document.getElementById('cloud-options').addEventListener('click', function() {
    document.getElementById('cloud-options').style.display = 'none';
});
