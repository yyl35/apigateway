FROM python:3.10

# 设置环境变量
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# 设置kubeconfig环境变量，kubectl和helm会使用这个配置文件


# 设置工作目录
WORKDIR /app

# 安装依赖
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/


COPY . /app/

# 创建.kube目录并复制kubeconfig


# 复制项目文件到容器


# 运行数据库迁移 (如果您有)

# 暴露端口
EXPOSE 8000
RUN chmod +x /app/start.sh
CMD ["/app/start.sh"]

