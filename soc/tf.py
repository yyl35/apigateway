import subprocess
import json
import uuid
import os
import shutil


def tf(base_dir):
        # 生成一个新的目录名
        new_dir = os.path.join(base_dir, str(uuid.uuid4()))
        os.makedirs(new_dir, exist_ok=True)

        # 复制TF脚本到新的目录
        shutil.copyfile(
            os.path.join(base_dir, "testtf.tf"),
            os.path.join(new_dir, "testtf.tf"),
        )

        # 指定terraform脚本的位置
        tf_plan_file = "plan.tfplan"

        # 初始化Terraform
        subprocess.check_call(['terraform', 'init', '-plugin-dir', 'C:\\projects\\djangoProject1\\soc\\scripts\\providers'], cwd=new_dir)

        # 计划Terraform变更并保存到文件
        subprocess.check_call(['terraform', 'plan', '-out=' + tf_plan_file], cwd=new_dir)

        # 应用Terraform变更
        subprocess.check_call(['terraform', 'apply', '-auto-approve', tf_plan_file], cwd=new_dir)

        # 获取公共IP
        output = subprocess.check_output(['terraform', 'output', '-json'], cwd=new_dir)
        output_dict = json.loads(output)
        instance_public_ip = output_dict['instance_public_ip']['value']

        print(f"The public IP of the instance is: {instance_public_ip}")

        # 删除 .terraform 文件夹
        shutil.rmtree(os.path.join(new_dir, ".terraform"))
        return instance_public_ip



