import base64
from kubernetes import client, config, utils
import yaml
import socket

def create_namespace(namespace_name):
    config.load_kube_config()
    v1 = client.CoreV1Api()

    namespace_body = client.V1Namespace(
        metadata=client.V1ObjectMeta(name=namespace_name)
    )

    try:
        v1.create_namespace(body=namespace_body)
        print(f"Namespace {namespace_name} created.")
    except client.ApiException as e:
        if e.status == 409:
            print(f"Namespace {namespace_name} already exists.")
        else:
            raise e


def apply_deployment(file_path, namespace):
    config.load_kube_config()
    with open(file_path) as f:
        docs = list(yaml.safe_load_all(f))

        for doc in docs:
            try:
                if doc["kind"] == "Deployment":
                    k8s_api = client.AppsV1Api()
                    resp = k8s_api.create_namespaced_deployment(
                        body=doc,
                        namespace=namespace
                    )
                    print(f"Deployment {doc['metadata']['name']} created. status={str(resp.status)}")

                elif doc["kind"] == "PersistentVolumeClaim":
                    k8s_api = client.CoreV1Api()
                    resp = k8s_api.create_namespaced_persistent_volume_claim(
                        body=doc, namespace=namespace
                    )
                    print(f"PVC {doc['metadata']['name']} created. status={str(resp.status)}")

                elif doc["kind"] == "Service":
                    k8s_api = client.CoreV1Api()
                    resp = k8s_api.create_namespaced_service(body=doc, namespace=namespace)
                    print(f"Service {doc['metadata']['name']} created. status={str(resp.status)}")

                # Add handling for ConfigMap
                elif doc["kind"] == "ConfigMap":
                    k8s_api = client.CoreV1Api()
                    resp = k8s_api.create_namespaced_config_map(body=doc, namespace=namespace)
                    print(f"ConfigMap {doc['metadata']['name']} created.")
            except client.ApiException as e:
                if e.status == 409:
                    print(f"{doc['kind']} {doc['metadata']['name']} already exists. Skipping...")
                else:
                    raise e

create_namespace('test-onepro')
apply_deployment('/opt/oneprovolume-httpd-images.yaml')
apply_deployment('/opt/oneprovolume-httpd-softwares.yaml')
apply_deployment('/opt/oneprovolume-influxdb.yaml')
apply_deployment('/opt/oneprovolume-log.yaml')
apply_deployment('/opt/oneprovolume-mysql.yaml')
apply_deployment('/opt/oneprovolume-rabbitmq.yaml')
apply_deployment('/opt/onepro02-redis-deployment.yaml')
apply_deployment('/opt/onepro05-httpd-deployment.yaml')
apply_deployment('/opt/onepro10-mysql-deployment.yaml')
apply_deployment('/opt/onepro11-rabbitmq-deployment.yaml')
apply_deployment('/opt/onepro12-influxdb-deployment.yaml')
apply_deployment('/opt/onepro29-sitter-deployment.yaml')
apply_deployment('/opt/onepro30-oneway-deployment.yaml')
apply_deployment('/opt/onepro31-unicloud-deployment.yaml')
apply_deployment('/opt/onepro32-proxy-deployment.yaml')
apply_deployment('/opt/onepro33-storplus-deployment.yaml')
apply_deployment('/opt/onepro34-porter-deployment.yaml')
apply_deployment('/opt/onepro35-owl-deployment.yaml')
apply_deployment('/opt/onepro36-revenue-deployment.yaml')
apply_deployment('/opt/onepro37-nezha-deployment.yaml')
apply_deployment('/opt/onepro38-mass-deployment.yaml')
apply_deployment('/opt/onepro39-crab-deployment.yaml')
apply_deployment('/opt/onepro41-newface-deployment.yaml')
apply_deployment('/opt/onepro42-ant-deployment.yaml')
apply_deployment('/opt/onepro43-atomy-api-deployment.yaml')
apply_deployment('/opt/onepro44-atomy-mistral-deployment.yaml')