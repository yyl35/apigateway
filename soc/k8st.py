import base64
from kubernetes import client, config ,utils
import yaml
import socket

def create_namespace(namespace_name):
    config.load_kube_config()
    v1 = client.CoreV1Api()

    namespace_body = client.V1Namespace(
        metadata=client.V1ObjectMeta(name=namespace_name)
    )

    try:
        v1.create_namespace(body=namespace_body)

        print(f"Namespace {namespace_name} created.")
    except client.ApiException as e:
        if e.status == 409:
            print(f"Namespace {namespace_name} already exists.")
        else:
            raise e

def apply_deployment(file_path):
    config.load_kube_config()
    with open("c:\\k8s\\dep-nginx.yaml") as f:
        dep = yaml.safe_load(f)
        k8s_api = client.AppsV1Api()
        resp = k8s_api.create_namespaced_deployment(
            body=dep,
            namespace="nginx-t")
        print(f"Deployment created. status={str(resp.status)}")

def apply_service(file_path):
    config.load_kube_config()
    k8s_client = client.ApiClient()
    utils.create_from_yaml(k8s_client, file_path)

def get_service(api_instance, service_name, namespace):
    return api_instance.read_namespaced_service(
        name=service_name,
        namespace=namespace
    )

# 应用 Deployment
#apply_service("c:\\k8s\\svc-nginx.yaml")
# apply_deployment("c:\\k8s\\dep-nginx.yaml")
#
#

# 获取服务地址



import subprocess


def helm_install(release_name, chart, namespace, additional_flags=None):
    command = ["helm", "install", release_name, chart, "-n", namespace]
    if additional_flags:
        command.extend(additional_flags)
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = process.communicate()

    if process.returncode != 0:
        print(f"Helm install command failed with: {error}")
    else:
        print(f"Helm install command output: {output}")



def helm_repo_add(repo_name, repo_url):
    command = f"helm repo add {repo_name} {repo_url}"
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()

    if process.returncode != 0:
        print(f"Helm repo add command failed with: {error}")
    else:
        print(f"Helm repo add command output: {output}")
        subprocess.check_call(["helm", "repo", "update"])

# 添加 Helm 仓库
helm_repo_add("opensearch", "https://opensearch-project.github.io/helm-charts/")
#
# # 安装 Elasticsearch Helm chart
import time

def wait_for_service_port(ip, port, timeout=600):
    start = time.time()
    while True:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect((ip, port))
                print(f"Service is exposed on https://{ip}:{port}")
                return
        except socket.error as e:
            if time.time() - start > timeout:
                print(f"Timeout when waiting for service at {ip}:{port}.")

                raise e
            else:
                time.sleep(5)

# 安装 Elasticsearch Helm chart
#helm_install("elasticsearch", "elastic/elasticsearch", "es", additional_flags=["--set", "antiAffinity=soft","--set", "service.type=NodePort"])
helm_install("opensearch", "opensearch/opensearch", "opensearch",additional_flags=["--set","service.type=NodePort"])
helm_install("opensearch-dashboards", "opensearch/opensearch-dashboards", "opensearch",additional_flags=["--set","service.type=NodePort"])

def create_service(service_file):
    config.load_kube_config()
    with open(service_file) as f:
        service = yaml.safe_load(f)
    k8s_api = client.CoreV1Api()
    resp = k8s_api.create_namespaced_service(
        body=service,
        namespace="es")
    print(f"Service created. status={str(resp.status)}")



def get_elastic_password(secret_name, namespace="es"):
    config.load_kube_config()
    v1 = client.CoreV1Api()
    secret = v1.read_namespaced_secret(secret_name, namespace)
    password = base64.b64decode(secret.data['password']).decode('utf-8')
    username = base64.b64decode(secret.data['username']).decode('utf-8')
    return username, password




# create_service('c:\\k8s\\svc-nginx.yaml
time.sleep(5)
# 等待服务准备好
config.load_kube_config()
v1 = client.CoreV1Api()
service_detail = get_service(v1, "opensearch-cluster-master",'opensearch')

for i in service_detail.spec.ports:
    if i.port == 9200 and i.node_port is not None:
        wait_for_service_port('114.96.88.34', i.node_port)
    elif i.port == 5601 and i.node_port is not None:
        wait_for_service_port('114.96.88.34', i.node_port)

# 获取密码
#username, password = get_elastic_password("elasticsearch-master-credentials")
#print(f"The username for the elastic search is {username} and the password is {password}")
print(f"The username for the elastic search is admin and the password is admin")






