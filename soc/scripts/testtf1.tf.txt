provider "alicloud" {
  access_key = "LTAI5tQ7JjeNMYzsrUvo3Dfw"
  secret_key = "0MWlY5AitpgriIjwrGv7qCNzOCmbHN"
  region     = "cn-shanghai"
}


# 生成随机名字
resource "random_pet" "name" {}

# 默认资源名称。
locals {
  production_name = "hyperbdr"
  new_scg_name = "sg-for-${local.production_name}"
  new_host_name = "app-for-${local.production_name}"
}

# 安全组基本信息配置
resource "alicloud_security_group" "default" {
  name        = local.new_scg_name
  description = "hyperbdr scg"
  vpc_id =  "vpc-uf616mg7xzsruqgjxla3l"
}



# 安全组入口端2
resource "alicloud_security_group_rule" "allow_web_1443" {
  security_group_id = "${alicloud_security_group.default.id}"
  type = "ingress"
  cidr_ip= "0.0.0.0/0"
  policy = "accept"
  ip_protocol= "tcp"
  port_range= "1443/1443"
  priority= 1
}

resource "alicloud_security_group_rule" "allow_web_8000" {
  security_group_id = "${alicloud_security_group.default.id}"
  type = "ingress"
  cidr_ip= "0.0.0.0/0"
  policy = "accept"
  ip_protocol= "tcp"
  port_range= "8000/8000"
  priority= 1
}

resource "alicloud_security_group_rule" "allow_web_9092" {
  security_group_id = "${alicloud_security_group.default.id}"
  type = "ingress"
  cidr_ip= "0.0.0.0/0"
  policy = "accept"
  ip_protocol= "tcp"
  port_range= "9092/9092"
  priority= 1
}

resource "alicloud_security_group_rule" "allow_web_8443" {
  security_group_id = "${alicloud_security_group.default.id}"
  type = "ingress"
  cidr_ip= "0.0.0.0/0"
  policy = "accept"
  ip_protocol= "tcp"
  port_range= "8443/8443"
  priority= 1
}

# 安全组出口端
resource "alicloud_security_group_rule" "allow_egress" {
  security_group_id = "${alicloud_security_group.default.id}"
  type = "egress"
  cidr_ip= "0.0.0.0/0"
  policy = "accept"
  ip_protocol= "tcp"
  port_range= "1/65535"
  priority= 1
}

# 实例基本配置
resource "alicloud_instance" "instance" {
  availability_zone = "cn-shanghai-b"
  security_groups = ["sg-uf60uymfy981eiecd6wl"]
  # series III
  host_name = "k3s"
  instance_type              = "ecs.c7.2xlarge"
  system_disk_size           = 250
  system_disk_category       = "cloud_essd"
  image_id                   = "m-uf61v2zwnsl3njln4jgx"
  vswitch_id                 = "vsw-uf6pyfkbysz36ecnuslrb"
  spot_strategy = "SpotAsPriceGo"
  internet_charge_type = "PayByTraffic"
  internet_max_bandwidth_out = 30
  instance_charge_type = "PostPaid"
  instance_name = "${random_pet.name.id}"
  user_data = "${data.template_file.user_data.rendered}"
}

data "template_file" "user_data" {
  template = <<EOF
#!/bin/bash
cd /root
yum install libaio -y
sh ngep_manager_V5.1.1_saas.20230427.bin
EOF
}

output "instance_public_ip" {
  value       = alicloud_instance.instance.public_ip
  description = "The public IP of the instance."
}

