from django.apps import AppConfig


class SocConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'soc'

AUTHLIB_OAUTH_CLIENTS = {
    'azure': {
        'client_id': 'your_client_id',
        'client_secret': 'your_client_secret',
        'access_token_url': 'https://login.microsoftonline.com/your_tenant_id/oauth2/token',
        'access_token_params': None,
        'authorize_url': 'https://login.microsoftonline.com/your_tenant_id/oauth2/authorize',
        'authorize_params': None,
        'api_base_url': 'https://graph.microsoft.com/v1.0/',
        'client_kwargs': {
            'scope': 'openid email profile',
        },
    },
}

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'authlib.integrations.django_client.backends.OAuthBackend',
]

LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

SESSION_COOKIE_SAMESITE = 'Lax'
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
