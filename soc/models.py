from django.db import models

# Create your models here.
class Userinfo(models.Model):
    name = models.CharField(max_length=32)
    password = models.CharField(max_length=64)
    age = models.IntegerField()


class ECSPrice(models.Model):
    region = models.CharField(max_length=100)
    instance_type = models.CharField(max_length=100)
    spot_price = models.FloatField()
    origin_price = models.FloatField()
    discount = models.FloatField()
    timestamp = models.DateTimeField()
    cpu_core_count = models.IntegerField()
    memory_size = models.FloatField()
