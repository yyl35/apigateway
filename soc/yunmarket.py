

import time
import pandas as pd
import json
from aliyunsdkcore.client import AcsClient
from aliyunsdkmarket.request.v20151101.DescribeProductRequest import DescribeProductRequest
from aliyunsdkmarket.request.v20151101.DescribeProductsRequest import DescribeProductsRequest

ak = 'LTAI5tA4oATzPxmLzNK3Sokm'
ck = 'A5n1PiaMAl4Uhn9aVNT1e11vJ41FpX'

client = AcsClient(ak, ck, 'cn-shanghai')

category_ids = [53366009, 52734001, 56764045, 56832009, 201204005, 205878902]

def get_all_product_codes(category_id):
    request = DescribeProductsRequest()
    request.set_accept_format('json')
    request.set_PageSize(50)
    request.set_PageNumber(1)
    request.add_query_param('Filter.1.Key', "categoryId")
    request.add_query_param('Filter.1.Value', category_id)
    products = {}

    while True:
        response = client.do_action_with_exception(request)
        response_json = json.loads(str(response, encoding='utf-8'))
        product_items = response_json['ProductItems']['ProductItem']

        for product in product_items:
            code = product['Code']
            product_info = {
                "CategoryId": product["CategoryId"],
                "WarrantyDate": product["WarrantyDate"],
                "ImageUrl": product["ImageUrl"],
                "TargetUrl": product["TargetUrl"],
                "OperationSystem": product["OperationSystem"],
                "DeliveryDate": product["DeliveryDate"],
                "Code": code,
                "ProductName": product["Name"],
                "ShortDescription": product["ShortDescription"],
                "PriceInfo": product["PriceInfo"],
                "SupplierId": product["SupplierId"],
                "DeliveryWay": product["DeliveryWay"],
                "Score": product["Score"],
                "SupplierName": product["SupplierName"],
                "SuggestedPrice": product["SuggestedPrice"],
                "Tags": product["Tags"]
            }
            products[code] = product_info

        if response_json['TotalCount'] == 0:
            break

        request.set_PageNumber(request.get_PageNumber() + 1)

    return products

def get_product_details(code):
    request = DescribeProductRequest()
    request.set_accept_format('json')
    request.set_Code(code)
    try:
        response = client.do_action_with_exception(request)
        response_dict = json.loads(str(response, encoding='utf-8'))

        shop_info = response_dict.get('ShopInfo', {})
        details = {
            "UserNames": [wangwang.get("UserName", "") for wangwang in shop_info.get("WangWangs", {}).get("WangWang", [])],
            "Remarks": [wangwang.get("Remark", "") for wangwang in shop_info.get("WangWangs", {}).get("WangWang", [])],
            "Telephones": shop_info.get("Telephones", {}).get("Telephone", []),
            "Emails": shop_info.get("Emails", ""),
            "ID": shop_info.get("Id", ""),
            "Name": shop_info.get("Name", "")
        }

        return details
    except Exception as e:
        print(e)
        return None

all_details = []
for category_id in category_ids:
    print(f"正在处理 CategoryId: {category_id}")
    all_products = get_all_product_codes(category_id)

    for index, (code, product_info) in enumerate(all_products.items(), 1):
        print(f"正在处理第 {index} 个产品，总共 {len(all_products)} 个产品.")
        details = get_product_details(code)
        if details:
            merged_info = {**product_info, **details}
            all_details.append(merged_info)

df = pd.DataFrame(all_details)
df.to_csv("products_details_combined.csv", index=False, encoding='utf-8-sig')

