import hmac
import base64
import hashlib
import json
import time
import uuid
import requests
from urllib.parse import urlparse

def sha256(content):
    x = hashlib.sha256()
    x.update(content.encode())
    return x.hexdigest().upper()

def hmac_sha256(key, content):
    sign = hmac.new(key, content, digestmod="sha256").digest()
    ret = base64.b64encode(sign)
    return ret

# 计算签名
def get_signature(ak, sk, params):
    # 创建待签名字符串
    # 一、header部分
    # 主要包括3个header需要作为签名内容：ctyun-eop-request-id、eop-date
    # 1. 首先通过uuid生成ctyun-eop-request-id
    request_id = str(uuid.uuid1())
    # 2. 获取当前时间戳并对时间进行格式化
    now_time = time.localtime()
    eop_date = time.strftime("%Y%m%dT%H%M%SZ", now_time)
    # 3. 对header部分按照字母顺序进行排序并格式化
    camp_header = "ctyun-eop-request-id:{0}\neop-date:{1}\n".format(request_id, eop_date)
  #  print(camp_header)

    parsed_url = urlparse(request_url)
    query = parsed_url.query
    query_params = sorted(query.split("&"))
    after_query = ""
    for query_param in query_params:
        if len(after_query) < 1:
            after_query += query_param
        else:
            after_query += "&" + query_param

    content_hash = sha256(json.dumps(params)).lower()

    # 完成创建待签名字符串
    pre_signature = camp_header + "\n"+ after_query + "\n" + content_hash

    # 构造动态密钥
    k_time = hmac_sha256(sk.encode("utf-8"), eop_date.encode("utf-8"))
    k_ak = hmac_sha256(base64.b64decode(k_time), ak.encode("utf-8"))
    k_date = hmac_sha256(base64.b64decode(k_ak), eop_date[:8].encode("utf-8"))

    # 签名的使用
    signature = hmac_sha256(base64.b64decode(k_date), pre_signature.encode("utf-8"))

    # 将数据整合得到真正的header中的内容
    sign_header = "{0} Headers=ctyun-eop-request-id;eop-date Signature={1}".format(ak, signature.decode())

    # 返回request-id eop-date和sign_header
    return request_id, eop_date, sign_header


# 向服务发送请求
def do_post(url, headers, params):
    response = requests.post(url, data=json.dumps(params), headers=headers)
    try:
        print(response.content)
        print(response.status_code)
        print(response.json())
    except AttributeError:
        print("请求失败")


def do_get(url, headers, params):
    response = requests.get(url, data=json.dumps(params), headers=headers)
    try:
        print(response.status_code)
        print(response.json())
    except AttributeError:
        print("请求失败")


if __name__ == '__main__':
    # 请求地址
    request_url = "https://ctecs-global.ctapi.ctyun.cn/v4/ecs/create-instance"
    # 官网accessKey
    request_url2 = "https://ctecs-global.ctapi.ctyun.cn/v4/common/get-ecs-flavors"
    ctyun_ak = "3c8c910fbb27487a92a54c96d021a7c4"
    ctyun_sk = "2f026551349643eebb642012dd0f95e8"
    ctyun_ak2 ="2fc6810ec84840f4bfba63657f3a2d40"
    ctyun_sk2 = "52b4fd5847a14c0d82638fef25a888b5"

    # body内容从本地文件中获取
    # 打开图片文件
    script = '''
#include
/home/test.sh
'''


    encoded_userdata = base64.b64encode(script.encode()).decode()

    print(encoded_userdata)
    params={
    "regionID": "a17034a4794111eaaa590242ac110002",
    "azName": "default",
    "instanceName": "create-instance-test",
    "displayName": "create-instance-test",
 #   "flavorID": "30b56e25-55e2-466a-b0d8-4c8474e778bc",
    "flavorID": "30b56e25-55e2-466a-b0d8-4c8474e778bc",
    "imageType": 0,
    "imageID": "51a00c73-a5de-4944-b3ae-35ebd2859f72",
    "vpcID": "d7c71571-409a-428a-b0eb-131422c70683",
    "bootDiskType": "SATA",
    "bootDiskSize": 40,
    "extIP": "1",
    "bandwidth": 2 ,
  #  "eipID": "05612958-6e2e-451e-ad01-a16b75162fd9",
    "onDemand": True,
    'cycleCount': 1,
    "userData": encoded_userdata,
    "cycleType": "MONTH",
    "autoRenewStatus": 0,
  #  "keyPairID":"KeyPair-e685",
    "userPassword":"u4ct8r5e!",
    "networkCardList": [
        {
            "nicName": "default_subnet",
            "subnetID": "ec65bc6a-b069-48fa-a334-03df029e9726",
            "isMaster": True
        }
    ],
   # "dataDiskList": [
    #    {
    #        "diskName":"ebs.dataDisk",
     #       "diskType":"SATA",
     #       "diskSize":10
     #   }
 #   ],
    "clientToken": "fc6fa0de-ebe7-4168-9ea2-9c987c4705f3",
}

    params2 = {
        "regionID": "a17034a4794111eaaa590242ac110002"
    }
    # body内容


    # 调用get_signature方法获取签名
    request_id, eop_date, sign_header = get_signature(ctyun_ak2, ctyun_sk2, params)

    # 生成请求header
    # 请求header
    headers = {
        'Content-Type': 'application/json',
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
        'ctyun-eop-request-id': request_id,

    }

    headers2={
        'Content-Type': 'application/json;charset=UTF-8',
        'ctyun-eop-request-id': request_id,
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
    }

    print("请求头部:")
    print(headers)

    # 执行post请求
 #   do_post(request_url, headers, params)

def regions_details():
    url = "https://ctecs-global.ctapi.ctyun.cn/v4/ecs/regions/details"
    params={"cloudPlatformType": "0"}
    request_id, eop_date, sign_header = get_signature(ctyun_ak2, ctyun_sk2, params)
    headers = {
        'Content-Type': 'application/json',
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
        'ctyun-eop-request-id': request_id,

    }
    do_get(url, headers, params)

def regions_list():
    url = "https://iam-global.ctapi.ctyun.cn/v3/regions"
    params={"cloudPlatformType": 0}
    request_id, eop_date, sign_header = get_signature(ctyun_ak2, ctyun_sk2, params)
    headers = {
        'Content-Type': 'application/json',
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
        'ctyun-eop-request-id': request_id,

    }
    do_get(url, headers, params)

def get_ecs_flavors():
    url = "https://ctecs-global.ctapi.ctyun.cn/v4/common/get-ecs-flavors"
    regionid="a17034a4794111eaaa590242ac110002"
    params={"regionID": regionid}
    request_id, eop_date, sign_header = get_signature(ctyun_ak, ctyun_sk, params)
    headers = {
        'Content-Type': 'application/json',
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
        'ctyun-eop-request-id': request_id,

    }
    do_get(url,headers,params)

def get_instance_details():
    url = "https://ctecs-global.ctapi.ctyun.cn/v4/ecs/instance-details"
    regionid="a17034a4794111eaaa590242ac110002"
    instanceid="4eba1c14-c07c-44fc-8bf3-7a0a598d7238"
    azname="default"
    params={"regionID": regionid,"instanceID": instanceid,}
    request_id, eop_date, sign_header = get_signature(ctyun_ak, ctyun_sk, params)
    headers = {
        'Content-Type': 'application/json',
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
        'ctyun-eop-request-id': request_id,

    }
    do_get(url,headers,params)

def get_instance_list():
    url = "https://ctecs-global.ctapi.ctyun.cn/v4/ecs/list-instances"
    regionid="a17034a4794111eaaa590242ac110002"
    params={"regionID": regionid}
    request_id, eop_date, sign_header = get_signature(ctyun_ak, ctyun_sk, params)
    headers = {
        'Content-Type': 'application/json',
        'Eop-Authorization': sign_header,
        'eop-date': eop_date,
        'ctyun-eop-request-id': request_id,

    }
    do_post(url,headers,params)

do_post(request_url, headers, params)
#regions_list()

