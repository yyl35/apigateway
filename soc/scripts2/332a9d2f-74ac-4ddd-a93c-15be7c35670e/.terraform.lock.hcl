# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/alicloud" {
  version = "1.207.2"
  hashes = [
    "h1:5monZ0vZIgw+8g3ltrS0APj47W06mFQNpO9qcYMog/I=",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:3hjTP5tQBspPcFAJlfafnWrNrKnr7J4Cp0qB9jbqf30=",
  ]
}
