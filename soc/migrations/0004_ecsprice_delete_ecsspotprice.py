# Generated by Django 4.1.7 on 2023-07-06 03:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soc', '0003_alter_ecsspotprice_spot_price'),
    ]

    operations = [
        migrations.CreateModel(
            name='ECSPrice',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('region', models.CharField(max_length=100)),
                ('instance_type', models.CharField(max_length=100)),
                ('spot_price', models.FloatField()),
                ('origin_price', models.FloatField()),
                ('discount', models.FloatField()),
                ('timestamp', models.DateTimeField()),
                ('cpu_core_count', models.IntegerField()),
                ('memory_size', models.FloatField()),
            ],
        ),
        migrations.DeleteModel(
            name='ECSSpotPrice',
        ),
    ]
