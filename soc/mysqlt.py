from kubernetes import client, config, utils
import yaml

import base64
from kubernetes import client, config, utils
import yaml
import socket

def create_namespace(namespace_name):
    config.load_kube_config()
    v1 = client.CoreV1Api()

    namespace_body = client.V1Namespace(
        metadata=client.V1ObjectMeta(name=namespace_name)
    )

    try:
        v1.create_namespace(body=namespace_body)
        print(f"Namespace {namespace_name} created.")
    except client.ApiException as e:
        if e.status == 409:
            print(f"Namespace {namespace_name} already exists.")
        else:
            raise e


def apply_deployment(file_path, namespace):
    config.load_kube_config()
    with open(file_path) as f:
        docs = list(yaml.safe_load_all(f))

        for doc in docs:
            try:
                if doc["kind"] == "Deployment":
                    k8s_api = client.AppsV1Api()
                    resp = k8s_api.create_namespaced_deployment(
                        body=doc,
                        namespace=namespace
                    )
                    print(f"Deployment {doc['metadata']['name']} created. status={str(resp.status)}")

                elif doc["kind"] == "PersistentVolumeClaim":
                    k8s_api = client.CoreV1Api()
                    resp = k8s_api.create_namespaced_persistent_volume_claim(
                        body=doc, namespace=namespace
                    )
                    print(f"PVC {doc['metadata']['name']} created. status={str(resp.status)}")

                elif doc["kind"] == "Service":
                    k8s_api = client.CoreV1Api()
                    resp = k8s_api.create_namespaced_service(body=doc, namespace=namespace)
                    print(f"Service {doc['metadata']['name']} created. status={str(resp.status)}")

                # Add handling for ConfigMap
                elif doc["kind"] == "ConfigMap":
                    k8s_api = client.CoreV1Api()
                    resp = k8s_api.create_namespaced_config_map(body=doc, namespace=namespace)
                    print(f"ConfigMap {doc['metadata']['name']} created.")
            except client.ApiException as e:
                if e.status == 409:
                    print(f"{doc['kind']} {doc['metadata']['name']} already exists. Skipping...")
                else:
                    raise e

import time

# ... [其他代码不变]

def check_deployment_ready(deployment_name, namespace, timeout=300):
    config.load_kube_config()
    api_instance = client.AppsV1Api()

    start_time = time.time()
    while True:
        response = api_instance.read_namespaced_deployment(deployment_name, namespace)
        if response.status.replicas == response.status.ready_replicas:
            print(f"Deployment {deployment_name} is ready.")
            return True
        if time.time() - start_time > timeout:
            print(f"Timeout while waiting for deployment {deployment_name} to be ready.")
            return False
        time.sleep(10)

create_namespace('test-onepro')

# First deploy mysql PVC
apply_deployment('c:\\yaml\\volume-mysql.yaml', 'test-onepro')

# Deploy mysql Deployment and wait for it to be ready
apply_deployment('c:\\yaml\\10-mysql-deployment.yaml', 'test-onepro')
if not check_deployment_ready("mysql", "test-onepro"):  # replace "mysql-deployment-name" with the name of your MySQL deployment
    print("MySQL deployment was not successful. Exiting.")
    exit(1)

# Then deploy other resources

# ... [其余代码不变]


# Then deploy other resources

