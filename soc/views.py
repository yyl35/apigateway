from django.core.checks import messages
from django.shortcuts import render,HttpResponse
from django.http import JsonResponse
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ServerException
import json
from aliyunsdkecs.request.v20140526.DescribeInstanceTypesRequest import DescribeInstanceTypesRequest
from aliyunsdkecs.request.v20140526.DescribeSpotPriceHistoryRequest import DescribeSpotPriceHistoryRequest
from django.views.decorators.csrf import csrf_exempt
from .models import ECSPrice
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
import requests
  # Import the functions
import threading
# Create your views here.
ak = 'LTAI5tA4oATzPxmLzNK3Sokm'
ck = 'A5n1PiaMAl4Uhn9aVNT1e11vJ41FpX'

def index(request):
    return HttpResponse("Welcome")


def create_service_instance(request):
    # 获取请求参数
    token = request.GET.get("token")
    action = request.GET.get("action")
    ali_uid = request.GET.get("aliUid")
    service_instance_id = request.GET.get("serviceInstanceId")
    service_id= request.GET.get("serviceInstanceId")
    service_parameters = request.GET.get("serviceParameters")

    # 执行创建服务实例的逻辑
    # ...
    response_data = {}
    if action=='createServiceInstance':
    # 构建响应数据
        response_data = {
            "status": "created",
            "outputs": {
                "frontEndUrl": "http://yourdomain.com/",
                "adminUrl": "http://yourdomain.com/admin",
                "username": "admin",
                "password": "admin_password"
            }
        }

    elif action == 'deleteServiceInstance':
        response_data={
            "status": "deleted"
        }

    return JsonResponse(response_data)
def create_service(request):
    base_url = "http://1.1.0.93:8000/ff/createService/"
    params = {
        "action": "createServiceInstance",
        "serviceInstanceId": "si-bb6bac964ba14c9c86bd",
        "aliUid": "8888888888",
        "serviceId": "service-ea80a0a91e0f41fb91fa",
        "serviceParameters": {
            "RegionId": "cn-hangzhou",
            "cloudtype": "ALIYUN",
            "appname": "fortinet-gate",
            "NumberofCPUs": 2,
            "ServicePackage": "Enterprise",
            "VirtualDomains": 2
        },
        "token": "b24b1cff3bcd81881988d68c2b4ef819"
    }

    try:
        response = requests.get(base_url, params=params)
        return JsonResponse({"status": response.status_code, "content": response.text})
    except requests.exceptions.RequestException as error:
        return JsonResponse({"error": str(error)})

def ecs_price(instance_types_info):
    results = []

    for region, type_info in instance_types_info.items():
        client = AcsClient(ak, ck, region)
        request = DescribeSpotPriceHistoryRequest()
        request.set_accept_format('json')
        request.set_NetworkType("vpc")

        for instance_type, type_detail in type_info.items():
            try:
                request.set_InstanceType(instance_type)
                response = client.do_action_with_exception(request)
                str1 = str(response, encoding='utf-8')
                data = json.loads(str1)
                spot_prices = data['SpotPrices']['SpotPriceType']

                if spot_prices:
                    current_instance_type = spot_prices[-1]['InstanceType']
                    spot_price = float(spot_prices[-1]['SpotPrice'])
                    origin_price = float(spot_prices[-1]['OriginPrice'])
                    timestamp = spot_prices[-1]['Timestamp']
                    discount = round(spot_price / origin_price, 2) if spot_price != 0 else 0
                    cpu_core_count = type_detail['CpuCoreCount']
                    memory_size = type_detail['MemorySize']

                    # 使用update_or_create来插入或更新记录
                    ecs_price, created = ECSPrice.objects.update_or_create(
                        instance_type=instance_type,
                        region=region,
                        defaults={
                            'spot_price': spot_price,
                            'origin_price': origin_price,
                            'discount': discount,
                            'timestamp': timestamp,
                            'cpu_core_count': cpu_core_count,
                            'memory_size': memory_size
                        }
                    )


            except ServerException as e:
                # 您也可以根据需要将异常保存在数据库中或记录日志。
                print("Server Exception: ", e)


def ecs_type():
    arealist = ['cn-qingdao', 'cn-beijing', 'cn-zhangjiakou', 'cn-huhehaote', 'cn-wulanchabu', 'cn-hangzhou',
                'cn-shanghai', 'cn-nanjing', 'cn-fuzhou', 'cn-shenzhen', 'cn-heyuan', 'cn-guangzhou', 'cn-chengdu']

    instance_types = {}
    for region in arealist:
        client = AcsClient(ak, ck, region)
        request = DescribeInstanceTypesRequest()
        request.set_accept_format('json')

        response = client.do_action_with_exception(request)
        str1 = str(response, encoding='utf-8')
        data = json.loads(str1)
        types = data['InstanceTypes']['InstanceType']

        for i in types:
            instance_type = i['InstanceTypeId']
            cpu_core_count = i['CpuCoreCount']
            memory_size = i['MemorySize']
            if region not in instance_types:
                instance_types[region] = {}
            instance_types[region][instance_type] = {'CpuCoreCount': cpu_core_count, 'MemorySize': memory_size}
    return instance_types

def fetch_data(request):
    # Call ecs_type and ecs_price here
    instance_types_info = ecs_type()
    ecs_price(instance_types_info)
    return render(request, 'data_fetched.html')  # You can create a simple HTML page to indicate that data was fetched successfully

# Create a view to display data
def display_data(request):
    sort_by = request.GET.get('sort_by', 'discount')

    # Get the data from the database and sort it
    prices = ECSPrice.objects.all().order_by(sort_by)

    # Render the data in the template
    return render(request, 'price.html', {'prices': prices})

@csrf_exempt
def update_data(request):
    if request.method == 'POST':
        # Start the data fetching in a new thread
        update_thread = threading.Thread(target=fetch_and_store_data)
        update_thread.start()

        # Redirect back to the display page
        return redirect('display_data')

def fetch_and_store_data():
    # Your functions to fetch data and store in database
    instance_types_info = ecs_type()
    ecs_price(instance_types_info)

def vendor_cards(request):
    vendors = [
        {'name': 'Fortinet', 'logo': 'images/fortinet-logo.png'},
        {'name': '万博', 'logo': 'images/wanbo-logo.png'},
        # 更多供应商...
    ]
    return render(request, 'index.html')


def page_3_view(request):
    return render(request, 'page_3.html')

def login_view(request):
    if request.method == "POST":
        username = request.POST['用户名']
        password = request.POST['密码']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/index')
        else:
            pass
            # (request, 'Invalid username or password.')
    return render(request, 'login.html')

def ecoss(request):
    return render(request, 'Ecoss.html')

from django.http import JsonResponse
from soc.tf import tf


def create_instance(request):
    # 调用函数以创建新的实例
    base_dir = r"C:\projects\djangoProject1\soc\scripts"
    public_ip = tf(base_dir)

    # 返回含有IP地址的JSON响应
    return JsonResponse({'instance_public_ip': public_ip})

def create_instance2(request):
    # 调用函数以创建新的实例
    base_dir = r"C:\projects\djangoProject1\soc\scripts1"
    public_ip = tf(base_dir)

    # 返回含有IP地址的JSON响应
    return JsonResponse({'instance_public_ip': public_ip+":20043"})
