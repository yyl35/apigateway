# coding=utf-8

from aliyunsdkcore.client import AcsClient
from aliyunsdkcms.request.v20190101.DescribeSystemEventAttributeRequest import DescribeSystemEventAttributeRequest
from aliyunsdkbssopenapi.request.v20171214.QueryInstanceBillRequest import QueryInstanceBillRequest
from aliyunsdkecs.request.v20140526.DescribeInstanceStatusRequest import DescribeInstanceStatusRequest
from aliyunsdkvpc.request.v20160428.DescribeVpcsRequest import DescribeVpcsRequest
from aliyunsdkrds.request.v20140815.DescribeDBInstancesRequest import DescribeDBInstancesRequest
from aliyunsdkecs.request.v20140526.DescribeDisksRequest import DescribeDisksRequest
from aliyunsdkecs.request.v20140526.DescribeInstancesRequest import DescribeInstancesRequest
from aliyunsdkmarket.request.v20151101.DescribeLicenseRequest import DescribeLicenseRequest
from aliyunsdkmarket.request.v20151101.DescribeOrderRequest import DescribeOrderRequest
from aliyunsdkecs.request.v20140526.DescribeSpotPriceHistoryRequest import DescribeSpotPriceHistoryRequest
from aliyunsdkecs.request.v20140526.DescribeInstanceTypesRequest import DescribeInstanceTypesRequest
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkecs.request.v20140526.DescribeImagesRequest import DescribeImagesRequest
from aliyunsdkecs.request.v20140526.CopyImageRequest import CopyImageRequest

import pandas as pd
from aliyunsdkcore.request import CommonRequest
import json


path = 'Ali_API_Key.csv'
#df = pd.read_csv(path)
#df = df.to_json(orient='records')
#df = json.loads(df)

ak = 'LTAI5tA4oATzPxmLzNK3Sokm'
ck = 'A5n1PiaMAl4Uhn9aVNT1e11vJ41FpX'
ak2='LTAI5tQ7JjeNMYzsrUvo3Dfw'
ck2='0MWlY5AitpgriIjwrGv7qCNzOCmbHN'

ak3='LTAI5t67coF6e87FcuhhAL1J'
ck3='IvvED8HkO9RrnYk1qsiwnUduGbNUZM'
def instance_bill():
    data = []
    result = []
    total_pretax_amount = 0.0
    for i in df:
        client = AcsClient(i["AccessKeyId"], i["AccessKeySecret"], i["zone"])
        print(i)
        for k in range(5):
            request = QueryInstanceBillRequest()
            request.set_accept_format('json')
            request.set_BillingCycle("2021-07")
            request.set_PageNum(k + 1)
            request.set_PageSize(300)
            response = client.do_action_with_exception(request)
            data_json = json.loads(str(response, encoding='utf-8'))
            if len(data_json["Data"]["Items"]["Item"]) == 0:
                continue
            #    print(data_json["Data"]["Items"]["Item"])

            product_name = '弹性公网IP'
            for item in data_json["Data"]["Items"]["Item"]:
                #   print(item)
                result.append({"InstanceID": item["InstanceID"], "PretaxAmount": item["PretaxAmount"]})
                total_pretax_amount += item["PretaxAmount"]

                if item["ProductName"] in product_name:
                    resource_groups = item["ResourceGroup"] if item["ResourceGroup"] != "" else "/"

                    data.append({"InstanceID": item["InstanceID"], "vm_status": "/", "subscription_id": i["access"],
                                 "resourceGroups": resource_groups, "location": i["zone"], "Category": "public_ip"})

    return {"data": data, "result": result}


def instance_status_quest():
    client = AcsClient(ak, ck, 'cn-shanghai')
    request = DescribeInstanceStatusRequest()
    response = client.do_action_with_exception(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))
    return response


def describe_vpc():
    data = []
    for i in df:
        client = AcsClient(i["AccessKeyId"], i["AccessKeySecret"], i["zone"])
        for k in range(5):
            request = DescribeVpcsRequest()
            request.set_accept_format('json')
            request.set_PageNumber(k + 1)
            request.set_PageSize(50)
            response = client.do_action_with_exception(request)
            data_json = json.loads(str(response, encoding='utf-8'))
            if len(data_json["Vpcs"]["Vpc"]) == 0:
                continue
            for Vpc in data_json["Vpcs"]["Vpc"]:
                resource_groups = Vpc["ResourceGroupId"] if Vpc["ResourceGroupId"] != "" else "/"
                vpc_id = Vpc["VpcId"] if Vpc["VpcId"] != "" else Vpc["VpcName"]
                data.append({"InstanceID": vpc_id, "vm_status": "/", "subscription_id": i["access"],
                             "resourceGroups": resource_groups, "location": i["zone"], "Category": "network interface"})
    return data


def describe_db():
    data = []
    for i in df:
        print(i)
        client = AcsClient(i["AccessKeyId"], i["AccessKeySecret"], i["zone"])
        request = DescribeDBInstancesRequest()
        request.set_accept_format('json')
        request.set_PageSize(100)
        request.set_PageNumber(1)
        response = client.do_action_with_exception(request)
        data_json = json.loads(str(response, encoding='utf-8'))
        if len(data_json["Items"]["DBInstance"]) == 0:
            continue
        for db in data_json["Items"]["DBInstance"]:
            resource_groups = db["ResourceGroupId"] if db["ResourceGroupId"] != "" else "/"

            data.append({"InstanceID": db["DBInstanceId"], "vm_status": "/", "subscription_id": i["access"],
                         "resourceGroups": resource_groups, "location": i["zone"], "Category": db["Engine"]})
    return data


def describe_disk():
    data = []
    for i in df:
        client = AcsClient(i["AccessKeyId"], i["AccessKeySecret"], i["zone"])
        for k in range(8):
            request = DescribeDisksRequest()
            request.set_accept_format('json')
            request.set_PageNumber(k + 1)
            request.set_PageSize(100)
            response = client.do_action_with_exception(request)
            data_json = json.loads(str(response, encoding='utf-8'))
            if len(data_json["Disks"]["Disk"]) == 0:
                continue
            for disk in data_json["Disks"]["Disk"]:
                resource_groups = disk["ResourceGroupId"] if disk["ResourceGroupId"] != "" else "/"
                disk_name = disk["DiskId"] if disk["DiskId"] != "" else disk["DiskName"]

                data.append({"InstanceID": disk_name, "vm_status": "/", "subscription_id": disk["InstanceId"],
                             "resourceGroups": resource_groups, "location": i["zone"], "diskSize": disk["Size"],
                             "Category": "Disk"})
    return data


def data_join():
    data_result = pd.DataFrame(instance_bill()["result"])
    result = instance_status_quest() + instance_bill()["data"] + describe_vpc() + describe_db() + describe_disk()
    result = pd.DataFrame(result).fillna(0)
    result["area"] = "阿里云"
    result["bg"] = "Digital"

    result = result.merge(data_result, how="left", on="InstanceID").fillna(0.0)
    result.rename({'InstanceID': 'name',
                   'vm_status': 'vm_status',
                   'subscription_id': 'subscription_id',
                   'resourceGroups': 'resourceGroups',
                   'location': 'location',
                   'Category': 'Category',
                   'diskSize': 'diskSize',
                   'area': 'area',
                   'bg': 'bg',
                   'PretaxAmount': 'PretaxAmount'}, axis='columns', inplace=True)

    result.to_csv("aliapi_msg.csv", encoding="utf-8-sig", index=False)
    result = result.to_json(orient='records')
    result = json.loads(result)
    print("处理完毕")
    return result


def concats():
    ali = pd.read_csv("aliapi_msg.csv")
    ali.rename(columns={"PretaxAmount": '"扩展的成本 (ExtendedCost)"'}, inplace=True)
    azure = pd.read_csv("merge.csv")
    azure2 = pd.read_csv("merge2.csv")
    result = ali.append(azure)
    result = result.append(azure2)
    result['real-time-cpu'] = np.nan
    result.to_csv("concat.csv", encoding="utf-8-sig", index=False)


def groupby(ali):
    data_result = ali
    print("azure：：：：", data_result)
    # print(data_result)
    data = pd.read_csv("aliapi_msg.csv")
    data["location"] = "aliyun-" + data["location"]

    Disk = data.loc[data["Category"] == "Disk"]
    Disk = Disk.groupby(["Category", "location"]).sum()
    Disk = Disk.reset_index()
    Disk["diskSize"] = Disk["diskSize"] / 1024
    Disk = Disk.to_json(orient='records')
    for d in json.loads(Disk):
        data_result["diskInTB"][d["location"]] = d["diskSize"]

    network = data.loc[data["Category"] == "network interface"]
    network = network.groupby(["Category", "location"]).count()
    network = network.reset_index()
    network = network.to_json(orient='records')
    for n in json.loads(network):
        data_result["network"][n["location"]] = n["name"]

    publicIpAddress = data.loc[data["Category"] == "public_ip"]
    publicIpAddress = publicIpAddress.groupby(["Category", "location"]).count()
    publicIpAddress = publicIpAddress.reset_index()
    publicIpAddress = publicIpAddress.to_json(orient='records')
    for p in json.loads(publicIpAddress):
        data_result["publicIpAddress"][p["location"]] = p["diskSize"]

    webApp = data.loc[data["Category"] == "webApp"]
    webApp = webApp.groupby(["Category", "location"]).count()
    webApp = webApp.reset_index()
    webApp = webApp.to_json(orient='records')
    if webApp:
        data_result["webApp"]['aliyun-cn-shanghai'] = 0.0
        data_result["webApp"]['aliyun-cn-shenzhen'] = 0.0
    else:
        for w in json.loads(webApp):
            data_result["webApp"][w["location"]] = w["diskSize"]

    database = data.loc[(data["Category"] == "SQLServer") | (data["Category"] == "MySQL")]
    database = database.groupby(["location"]).count()
    database = database.reset_index()
    database = database.to_json(orient='records')
    for db in json.loads(database):
        data_result["database"][db["location"]] = db["diskSize"]

    digital = data.loc[
        (data["Category"] != "Disk") | (data["Category"] != "network interface") | (data["Category"] != "public_ip")]
    digital = digital["name"].count()
    data_result["digital"] += digital

    databill = data.groupby(["location"]).sum()
    databill = databill.reset_index()
    cost = databill["PretaxAmount"].sum()
    databill = databill.to_json(orient='records')

    data_result["preferential_price"] += cost

    for bill in json.loads(databill):
        data_result["bill"][bill["location"]] = bill["PretaxAmount"]

    vm = data.loc[(data["Category"] == "VM")]
    vmlocation = vm.groupby(["location"]).count()
    vmlocation = vmlocation.reset_index()
    vmlocation = vmlocation.to_json(orient='records')

    vmtotal = vm.groupby(["location", "vm_status"]).count()
    vmtotal = vmtotal.reset_index()
    # print(data_result)
    # vmtotal = vmtotal[{"location", "vm_status", "name"}]

    # print(vmtotal)
    vmtotals = vmtotal.to_json(orient='records')
    vmtotal = vmtotal.groupby(["location"]).sum()
    vmtotal = vmtotal.reset_index()
    #    vmtotal = vmtotal[{"location", "name"}]
    vmtotal = vmtotal.to_json(orient='records')
    for vl in json.loads(vmlocation):
        data_result["vm"][vl["location"]] = {}
        for vs in json.loads(vmtotals):
            if vl["location"] == vs["location"]:
                data_result["vm"][vl["location"]]["vm" + vs["vm_status"]] = vs["name"]
                for vt in json.loads(vmtotal):
                    if vl["location"] == vt["location"]:
                        data_result["vm"][vt["location"]]["vmTotal"] = vt["name"]

    print(data_result)
    return data_result


def describe_ecs():
    client = AcsClient(ak, ck, 'cn-hongkong')

    request = DescribeInstancesRequest()
    request.set_accept_format('json')

    response = client.do_action_with_exception(request)
    # python2:  print(response)
    str1 = str(response, encoding='utf-8')
    data = json.loads(str1)
   # price=price['Instances']['Instance'][0]['SpotPriceLimit']
    instances = data['Instances']['Instance']
    print(instances)
    dict={}
    for i in instances:
       ip = i['PublicIpAddress']['IpAddress']
       id= i['HostName']
       price=i['SpotPriceLimit']
       dict[id] = {'IP': ip, 'Price': price}
    print(dict)
    return dict

def describe_license():
    client = AcsClient(ak, ck, 'cn-beijing')
    request = DescribeLicenseRequest()
    request.set_accept_format('json')
    request.set_LicenseCode("TW1NPPLG4L2INT-IQWSLYQETKCVZ7DSBUQUV8S9QDH0R0FG7MSL03G8NMFCYUMV3")
    # python2:  print(response)
    request.set_accept_format('json')
    response = client.do_action_with_exception(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))

def describe_ecs_event():
    client = AcsClient(ak, ck, 'cn-hongkong')

    request = DescribeSystemEventAttributeRequest()
    request.set_accept_format('json')

    response = client.do_action_with_exception(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))

def describe_market_order():

    client = AcsClient(ak, ck, 'cn-beijing')
    request = DescribeOrderRequest()
    request.set_accept_format('json')

    request.set_OrderId("224731509050707")

    response = client.do_action_with_exception(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))


def ecs_price(instance_types_info):
    results = []
    for region, type_info in instance_types_info.items():
        client = AcsClient(ak, ck, region)
        request = DescribeSpotPriceHistoryRequest()
        request.set_accept_format('json')
        request.set_NetworkType("vpc")

        for instance_type, type_detail in type_info.items():
            try:
                request.set_InstanceType(instance_type)
                response = client.do_action_with_exception(request)
                str1 = str(response, encoding='utf-8')
                data = json.loads(str1)
                spot_prices = data['SpotPrices']['SpotPriceType']

                if spot_prices:
                    instance_type = spot_prices[-1]['InstanceType']
                    spot_price = float(spot_prices[-1]['SpotPrice'])
                    origin_price = float(spot_prices[-1]['OriginPrice'])
                    timestamp = spot_prices[-1]['Timestamp']
                    discount = round(spot_price / origin_price, 2) if spot_price != 0 else 0
                    cpu_core_count = type_detail['CpuCoreCount']
                    memory_size = type_detail['MemorySize']
                    results.append({'Region': region, 'InstanceType': instance_type, 'SpotPrice': spot_price, 'OriginPrice': origin_price,
                                    'Discount': discount, 'Time': timestamp, 'CPU': cpu_core_count, 'Memory': memory_size})

            except ServerException as e:
                results.append({'Region': region, 'InstanceType': instance_type, 'SpotPrice': 'Not Supported', 'OriginPrice': 'Not Supported',
                                'Discount': 'Not Supported', 'Time': 'Not Supported', 'CPU': 'Not Supported', 'Memory': 'Not Supported'})

    # Use pandas to create a DataFrame and write to CSV
    df = pd.DataFrame(results)
    df.to_csv('prices.csv', index=False)


def describe_image(region):
    client = AcsClient(ak3, ck3, region)
    request = DescribeImagesRequest()
    request.set_accept_format('json')

    request.set_PageNumber(1)
    request.set_PageSize(100)
    request.set_ImageOwnerAlias("self")
   # request.set_ImageOwnerId(1208381701894055)

    response = client.do_action_with_exception(request)
    response_data = json.loads(str(response, encoding='utf-8'))
    return response_data["Images"]["Image"]


from aliyunsdkcore.acs_exception.exceptions import ServerException

def copy_image(image_id, image_name, destination_region):
    client = AcsClient(ak3, ck3, region)
    request = CopyImageRequest()
    request.set_accept_format('json')
    request.set_DestinationRegionId(destination_region)
    request.set_ImageId(image_id)
    request.set_DestinationImageName(image_name)

    try:
        response = client.do_action_with_exception(request)
        print(str(response, encoding='utf-8'))
    except ServerException as e:
        # Check if the error is related to duplicate image name
        if e.error_code == "InvalidImageName.Duplicated":
            print(f"Image {image_name} ({image_id}) already exists in {destination_region}. Skipping...")
        else:
            # If it's another error, re-raise the exception
            raise e


def ecs_type():
    arealist = ['cn-qingdao', 'cn-beijing', 'cn-zhangjiakou', 'cn-huhehaote', 'cn-wulanchabu', 'cn-hangzhou',
                'cn-shanghai', 'cn-nanjing', 'cn-fuzhou', 'cn-shenzhen', 'cn-heyuan', 'cn-guangzhou', 'cn-chengdu']

    instance_types = {}
    for region in arealist:
        client = AcsClient(ak, ck, region)
        request = DescribeInstanceTypesRequest()
        request.set_accept_format('json')

        response = client.do_action_with_exception(request)
        str1 = str(response, encoding='utf-8')
        data = json.loads(str1)
        types = data['InstanceTypes']['InstanceType']

        for i in types:
            instance_type = i['InstanceTypeId']
            cpu_core_count = i['CpuCoreCount']
            memory_size = i['MemorySize']
            if region not in instance_types:
                instance_types[region] = {}
            instance_types[region][instance_type] = {'CpuCoreCount': cpu_core_count, 'MemorySize': memory_size}
    return instance_types


def create_service():
    client = AcsClient(ak2, ck2, 'cn-hangzhou')
    request = CommonRequest()
    request.set_accept_format('json')
   # request.set_domain('computenestsupplier.cn-hangzhou.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2021-05-21')
    request.set_action_name('CreateService')
    request.set_domain('computenestsupplier.cn-hangzhou.aliyuncs.com')
    request.add_query_param('DeployType', "terraform")
    request.add_query_param('ServiceType', "managed")
    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('ServiceInfo.1.Locale', "zh-CN")
    request.add_query_param('ServiceInfo.1.Name', "autest")
    request.add_query_param('ServiceInfo.1.ShortDescription', "autest")
    request.add_query_param('ServiceInfo.1.Image', "http://autest74.oss-cn-beijing.aliyuncs.com/20191218173156.jpg")
    request.add_query_param('PolicyNames', "autest")
    request.add_query_param('Duration', "0")
    request.add_query_param('ShareType', "Public")
    request.add_query_param('TrialDuration', "7")
    request.add_query_param('DeployMetadata', '''{
        "TemplateConfigs": [
            {
                "Name": "模板1",
                "Url": "oss://computenest-test/template.json?RegionId=cn-beijing",
                "PredefinedParameters": [
                    {
                        "Name": "低配版",
                        "Parameters": {
                            "InstanceType": "ecs.g5.large",
                            "DataDiskSize": 40
                        }
                    },
                    {
                        "Name": "高配版",
                        "Parameters": {
                            "InstanceType": "ecs.g5.large",
                            "DataDiskSize": 200
                        }
                    }
                ]
            }
        ]
    }''')

    response = client.do_action_with_exception(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))


if __name__ == '__main__':
    region = 'cn-shanghai'
    images = describe_image(region)

    arealist = ['cn-shanghai','cn-qingdao', 'cn-beijing', 'cn-zhangjiakou', 'cn-huhehaote', 'cn-wulanchabu', 'cn-hangzhou',
                'cn-shanghai', 'cn-nanjing', 'cn-fuzhou', 'cn-shenzhen', 'cn-heyuan', 'cn-guangzhou', 'cn-chengdu']
    arealist2=['cn-shanghai']
    for image in images:
        image_id = image["ImageId"]
        image_name = image["ImageName"]
        for area in arealist:
            print(f"Copying image {image_name} ({image_id}) to {area}...")
            copy_image(image_id, image_name, area)

