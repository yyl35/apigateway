# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/alicloud" {
  version = "1.207.2"
  hashes = [
    "h1:5monZ0vZIgw+8g3ltrS0APj47W06mFQNpO9qcYMog/I=",
    "zh:16b96101254d6f119cc45634db60025ebc93c5f3bfba56a829e7c9f016ba6bdd",
    "zh:170671ee915cd20853a3697ecf87eb67d23df2660c12ca313e38582ffd32eceb",
    "zh:1ecfa86da5a254b01b9788e74e425a3bee9bef8edfe7290acbd5caa3efc1f38b",
    "zh:2d0eed1d8f05690b56174e5b02b5c4a6fac1b1be778ba81acb47d2b6f09718f0",
    "zh:5de731c07a10af4432ab4569431c106c61391ecf63eb86e62d3ec3463c72c726",
    "zh:7e350e0e1b9f2069029665732bf4369be6fe40eef13403e7f8db8d803e48ce2a",
    "zh:89cadd956daebf7605a4017824eec18206841ca1b346d27a850203116130b229",
    "zh:b52cc3b5efc6dcd694521dc3890eac91ac32176e46cafc62ccf3b9bf7b83107e",
    "zh:da4c924ff1b9e06feda1bae2042f3308fefe5a9c4ced97088fbe636502c52f0f",
    "zh:e55903567e3c036eaebdd1e10b43c40ff2734c4f9dff987923030403d7cdb686",
    "zh:f0e331db75b0bfc03fe6eaa4ec8f83eb898dfbec277abe013110bbde78f6f244",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:3hjTP5tQBspPcFAJlfafnWrNrKnr7J4Cp0qB9jbqf30=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:LN84cu+BZpVRvYlCzrbPfCRDaIelSyEx/W9Iwwgbnn4=",
  ]
}
